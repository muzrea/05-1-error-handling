package com.twuc.webApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ErrorController {

    @GetMapping("/api/errors/default")
    Error getRuntimeExceptionDefaultStatus(){
        throw new RuntimeException("runtime exception");
    }

    @GetMapping("/api/errors/illegal-argument")
    public ResponseEntity<String> getIllegalArgumentException (){
        throw new IllegalArgumentException();
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> getAllIllegalArgumentExceptionReturn400(){
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body("Something wrong with the argument");
    }

    @GetMapping("/api/errors/null-pointer")
    Error getNullPointException(){
        throw new NullPointerException("null-pointer exception");
    }

    @GetMapping("/api/errors/arithmetic")
    Error getArithmeticException(){
        throw new ArithmeticException("arithmetic exception");
    }

    @ExceptionHandler({NullPointerException.class,ArithmeticException.class})
    public ResponseEntity<String> nullPointAndArithmeticException (){
        return ResponseEntity.status(418)
                .body("Something wrong with the argument");
    }
}
