package com.twuc.webApp.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ControllerAdvice
public class IllgegalArgumentExceptionController {
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> getAllIllegalArgumentException(){
        return ResponseEntity.status(418)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body("{\"message\": \"Something wrong with brother or sister.\"}");
    }
}
