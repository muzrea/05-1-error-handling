package com.twuc.webApp.errorController;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureWebClient
public class ErrorControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    void should_throw_runtime_exception() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/errors/default", String.class);
        assertEquals(500,forEntity.getStatusCodeValue());
    }

    @Test
    void should_throw_IllegalArgumentException() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/errors/illegal-argument", String.class);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,forEntity.getStatusCode());
        assertEquals("Something wrong with the argument",forEntity.getBody());
    }

    @Test
    void should_return_something_wrong_with_the_argument_when_throw_nullPointerException() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/errors/null-pointer", String.class);
        assertEquals(418,forEntity.getStatusCodeValue());
        assertEquals("Something wrong with the argument",forEntity.getBody());

    }

    @Test
    void should_return_something_wrong_with_the_argument_when_throw_arithmeticException() throws Exception {
        ResponseEntity<String> entity = testRestTemplate.getForEntity("/api/errors/arithmetic", String.class);
        assertEquals(418,entity.getStatusCodeValue());
        assertEquals("Something wrong with the argument",entity.getBody());
    }

    @Test
    void should_return_message_when_throw_IllegalArgumentException_api_is_sister() throws Exception {
        ResponseEntity<String> entity = testRestTemplate.getForEntity("/api/sister-errors/illegal-argument", String.class);
        assertEquals(418,entity.getStatusCodeValue());
        assertEquals("{\"message\": \"Something wrong with brother or sister.\"}",entity.getBody());
    }

    @Test
    void should_return_message_when_throw_IllegalArgumentException_api_is_brother() throws Exception {
        ResponseEntity<String> entity = testRestTemplate.getForEntity("/api/brother-errors/illegal-argument", String.class);
        assertEquals(418,entity.getStatusCodeValue());
        assertEquals("{\"message\": \"Something wrong with brother or sister.\"}",entity.getBody());
    }
}

