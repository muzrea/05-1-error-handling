package com.twuc.webApp.errorController;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureWebClient
public class ErrorControllerTestExercise {
    @Autowired
    TestRestTemplate testRestTemplate;
    @Test
    void should_throw_runtime_exception_use_template() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/exceptions/runtime-exception", String.class);
        assertEquals(HttpStatus.NOT_FOUND,forEntity.getStatusCode());
    }

    @Test
    void should_throw_runtime_exception_use_() throws Exception {
        ResponseEntity<String> forEntity = testRestTemplate.getForEntity("/api/exceptions/access-control-exception", String.class);
        assertEquals(HttpStatus.NOT_FOUND,forEntity.getStatusCode());
    }
}
